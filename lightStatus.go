package main

import (
	"encoding/xml"
	ctools "gitlab.com/appf-anu/chamber-tools"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type lightStatusXMLRepresentation struct {
	LightTime         string `xml:"a"`
	ScheduleStatus    string `xml:"b"`
	LightStatus       string `xml:"c"`
	Uptime            string `xml:"d"`
	LastChangeTime    string `xml:"e"`
	LastChangeReason  string `xml:"f"`
	LastChangeIP      string `xml:"g"`
	LastChangeType    string `xml:"h"`
	PanelTemperatures string `xml:"i"`
	Intensities       string `xml:"j"`
	ControlMode       string `xml:"m"`
	UIValues1         string `xml:"n"`
	UIValues2         string `xml:"o"`
	NTPInfo           string `xml:"q"`
}

/*
s7:
	channel-1 400nm
	channel-2 420nm
	channel-3 450nm
	channel-4 530nm
	channel-5 630nm
	channel-6 660nm
	channel-7 735nm
s10:
	channel-1 370nm
	channel-2 400nm
	channel-3 420nm
	channel-4 450nm
	channel-5 530nm
	channel-6 620nm
	channel-7 660nm
	channel-8 735nm
	channel-9 850nm
	channel-10 6500k
dyna:
	channel-1 380nm
	channel-2 400nm
	channel-3 420nm
	channel-4 450nm
	channel-5 530nm
	channel-6 620nm
	channel-7 660nm
	channel-8 735nm
	channel-9 5700K
*/

type lightStatus struct {
	LightTime              time.Time
	ScheduleStatus         bool
	LightStatus            bool
	Uptime                 time.Duration
	LastChangeTime         time.Time
	LastChangeReason       string
	LastChangeIP           string
	LastChangeType         string
	PanelTemperatureC      []float64
	Wavelength380nm        float64
	Wavelength380nmTarget  float64
	Wavelength400nm        float64
	Wavelength400nmTarget  float64
	Wavelength420nm        float64
	Wavelength420nmTarget  float64
	Wavelength450nm        float64
	Wavelength450nmTarget  float64
	Wavelength530nm        float64
	Wavelength530nmTarget  float64
	Wavelength630nm        float64
	Wavelength630nmTarget  float64
	Wavelength660nm        float64
	Wavelength660nmTarget  float64
	Wavelength735nm        float64
	Wavelength735nmTarget  float64
	Wavelength370nm        float64
	Wavelength370nmTarget  float64
	Wavelength620nm        float64
	Wavelength620nmTarget  float64
	Wavelength850nm        float64
	Wavelength850nmTarget  float64
	Wavelength5700k        float64
	Wavelength5700kTarget  float64
	Wavelength6500k        float64
	Wavelength6500kTarget  float64
	ControlMode            string
	UILightsOnAtPowerUp    bool
	UIStatusIndicatorLed   bool
	UIScheduleLockOn       bool
	UIScheduleLockMessage  string
	UIScheduleLockPassword string
	NTPStatus              bool
	NTPAddress             string
	TimeZoneOffset         string
	ExecutedTimepoint      bool
}

func newLightStatusPtr() *lightStatus {
	ls := &lightStatus{
		time.Time{},
		false,
		false,
		time.Duration(0),
		time.Time{},
		"",
		"",
		"",
		[]float64{},
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		"",
		false,
		false,
		false,
		"",
		"",
		false,
		"",
		"",
		false,
	}
	return ls
}

// Unmarshal unmarshals the xml representation of the response from the light into a lightStatus struct
func (ls *lightStatus) Unmarshal(data []byte) error {
	xmlrep := lightStatusXMLRepresentation{}
	err := xml.Unmarshal(data, &xmlrep)
	if err != nil {
		errLog.Printf("error decoding status.xml: %v\n", err)
		return err
	}

	if xmlrep.LightTime != "" {
		//  light time
		lightTime, err := time.Parse("2006:01:02:15:04:05", xmlrep.LightTime)
		if err != nil {
			errLog.Printf("error decoding status.xml: %v\n", err)
		} else {
			ls.LightTime = lightTime
		}
	}

	if xmlrep.ScheduleStatus != "" {
		// schedule status
		if xmlrep.ScheduleStatus == "Running" {
			ls.ScheduleStatus = true
		} else {
			ls.ScheduleStatus = false
		}
	}

	if xmlrep.LightStatus != "" {
		// light status
		if xmlrep.LightStatus == "OK" {
			ls.LightStatus = true
		} else {
			ls.LightStatus = false
		}
	}

	if xmlrep.Uptime != "" {
		// uptime
		uptimeStr := strings.Replace(xmlrep.Uptime, " ", "", -1)
		uptimeStrSplit := strings.Split(uptimeStr, "d")
		uptimeDuration, err := time.ParseDuration(uptimeStrSplit[len(uptimeStrSplit)-1])
		if err != nil {
			errLog.Printf("error decoding status.xml: %v\n", err)
		} else {
			if len(uptimeStrSplit) > 1 {
				if durationDays, err := strconv.ParseInt(uptimeStrSplit[0], 10, 64); err != nil {
					errLog.Printf("error decoding status.xml: %v\n", err)
					uptimeDuration = -1 // we error so set duration to -1 to avoid incomplete value
				} else {
					hours := 24 * durationDays
					uptimeDuration += time.Duration(hours) * time.Hour
				}
			}
			if uptimeDuration > 0 {
				ls.Uptime = uptimeDuration
			}
		}
	}

	if xmlrep.LastChangeTime != "" {
		// last changed time
		space := regexp.MustCompile(`\s+`)
		lastChangedTime, err := time.Parse("2006-01-02T15:04:05", space.ReplaceAllString(xmlrep.LastChangeTime, "T"))
		if err != nil {
			errLog.Printf("error decoding status.xml: %v\n", err)
		} else {
			ls.LastChangeTime = lastChangedTime
		}
	}

	ls.LastChangeReason = xmlrep.LastChangeReason
	ls.LastChangeIP = xmlrep.LastChangeIP
	ls.LastChangeType = xmlrep.LastChangeType

	if xmlrep.PanelTemperatures != "" {
		// panel temperatures
		temperatureValuesStr := strings.TrimSuffix(xmlrep.PanelTemperatures, ",")
		temperatureValues := strings.Split(temperatureValuesStr, ",")
		for _, tempStr := range temperatureValues {
			tval := strings.Split(tempStr, ":")
			tempStr = tval[len(tval)-1]
			tempUnit := tempStr[len(tempStr)-1:]
			tempValueStr := tempStr[:len(tempStr)-2]
			tempValue, err := strconv.ParseFloat(tempValueStr, 10)
			if err != nil {
				errLog.Printf("error decoding status.xml: %v\n", err)
			} else {
				// if temperature is in freedom units, convert to something that is useful in the real world
				if tempUnit == "F" {
					tempValue = 5.0 / 9.0 * (tempValue - 32.0)
				}
				ls.PanelTemperatureC = append(ls.PanelTemperatureC, tempValue)
			}
		}
	}
	if xmlrep.Intensities != "" {
		// light intensities
		intensityValuesStr := strings.TrimSuffix(xmlrep.Intensities, ",")
		intensityValues := strings.Split(intensityValuesStr, ",")
		getValue := func(n int) float64 {
			intStr := intensityValues[0]
			ival := strings.Split(intStr, ":")
			intStr = ival[len(ival)-1]
			intensityValue, err := strconv.ParseInt(intStr, 10, 64)
			if err != nil {
				errLog.Println(err)
				return ctools.NullTargetFloat64
			}
			// values are returned as integers up to 1000
			//convert this to percentage.
			return float64(intensityValue) / 10.0
		}
		switch len(intensityValues) {
		case len(wavelengthsDyna):
			/*dyna:
			channel-1 380nm
			channel-2 400nm
			channel-3 420nm
			channel-4 450nm
			channel-5 530nm
			channel-6 620nm
			channel-7 660nm
			channel-8 735nm
			channel-9 5700K
			*/
			ls.Wavelength380nm = getValue(0)
			ls.Wavelength400nm = getValue(1)
			ls.Wavelength420nm = getValue(2)
			ls.Wavelength450nm = getValue(3)
			ls.Wavelength530nm = getValue(4)
			ls.Wavelength620nm = getValue(5)
			ls.Wavelength660nm = getValue(6)
			ls.Wavelength735nm = getValue(7)
			ls.Wavelength5700k = getValue(8)

		case len(wavelengthsS10):
			/*s10:
			channel-1 370nm
			channel-2 400nm
			channel-3 420nm
			channel-4 450nm
			channel-5 530nm
			channel-6 620nm
			channel-7 660nm
			channel-8 735nm
			channel-9 850nm
			channel-10 6500k
			*/
			ls.Wavelength370nm = getValue(0)
			ls.Wavelength400nm = getValue(1)
			ls.Wavelength420nm = getValue(2)
			ls.Wavelength450nm = getValue(3)
			ls.Wavelength530nm = getValue(4)
			ls.Wavelength620nm = getValue(5)
			ls.Wavelength660nm = getValue(6)
			ls.Wavelength735nm = getValue(7)
			ls.Wavelength850nm = getValue(7)
			ls.Wavelength6500k = getValue(8)

		case len(wavelengthsS7):
			/*s7:
			channel-1 400nm
			channel-2 420nm
			channel-3 450nm
			channel-4 530nm
			channel-5 630nm
			channel-6 660nm
			channel-7 735nm
			*/
			ls.Wavelength400nm = getValue(0)
			ls.Wavelength420nm = getValue(1)
			ls.Wavelength450nm = getValue(2)
			ls.Wavelength530nm = getValue(3)
			ls.Wavelength630nm = getValue(4)
			ls.Wavelength660nm = getValue(5)
			ls.Wavelength735nm = getValue(6)

		default:
			errLog.Printf("got incorrect number of intensities from device: %d\n", len(intensityValues))
		}
	}
	// ignore 10, and 11.
	ls.ControlMode = xmlrep.ControlMode

	if xmlrep.UIValues1 != "" {
		uiValues1 := make([]string, 3)

		copy(uiValues1, strings.Split(xmlrep.UIValues1, ":"))
		// uiValues[0] here is the temperature unit, we dont need this.

		if uiValues1[1] == "on" {
			ls.UILightsOnAtPowerUp = true
		} else if uiValues1[1] == "off" {
			ls.UILightsOnAtPowerUp = false
		}

		// for some reason this value is actually indicated true by the string "normal" instead of "on", wtf.
		if uiValues1[2] == "normal" {
			ls.UIStatusIndicatorLed = true
		} else if uiValues1[2] == "off" {
			ls.UIStatusIndicatorLed = false
		}
	}

	if xmlrep.UIValues2 != "" {
		uiValues2 := make([]string, 3)
		copy(uiValues2, strings.Split(xmlrep.UIValues2, ":"))
		if uiValues2[0] == "on" {
			ls.UIScheduleLockOn = true
		} else if uiValues2[0] == "off" {
			ls.UIScheduleLockOn = false
		}
		if uiValues2[1] != "" {
			ls.UIScheduleLockMessage = uiValues2[1]
		}
		if uiValues2[2] != "" {
			ls.UIScheduleLockPassword = uiValues2[2]
		}
	}

	// ignore 15
	if xmlrep.NTPInfo != "" {
		ntpValues := make([]string, 3)
		// for some reason ntp info is comma-space separated <shrug>
		copy(ntpValues, strings.Split(xmlrep.NTPInfo, ", "))
		if ntpValues[0] == "on" {
			ls.NTPStatus = true
		} else if ntpValues[0] == "off" {
			ls.NTPStatus = false
		}
		if ntpValues[1] != "" {
			ls.NTPAddress = ntpValues[1]
		}
		if ntpValues[2] != "" {
			ls.TimeZoneOffset = ntpValues[2]
		}
	}
	// we arent going to bother with the wifi settings.
	return nil
}
