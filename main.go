package main

import (
	"flag"
	"fmt"
	ctools "gitlab.com/appf-anu/chamber-tools"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	errLog *log.Logger
)

var (
	noMetrics, dummy, loopFirstDay, disco bool
	address                               string
	statusURL, intensityURL               *url.URL
	multiplier                            float64
	conditionsPath                        string
	tags                                  ctools.TelegrafTags
	interval                              time.Duration
)

var usage = func() {
	use := `
usage of %s:
flags:
	-no-metrics: don't send metrics to telegraf
	-dummy: don't control the chamber, only collect metrics (this is implied by not specifying a conditions file
	-conditions: conditions to use to run the chamber
	-interval: what interval to run conditions/record metrics at, set to 0s to read 1 metric and exit. (default=10m)

examples:
	collect data on 192.168.1.3  and output the errors to GC03-error.log and record the output to GC03.log
	%s -dummy 192.168.1.3 2>> GC03-error.log 1>> GC03.log

	run conditions on 192.168.1.3  and output the errors to GC03-error.log and record the output to GC03.log
	%s -conditions GC03-conditions.csv -dummy 192.168.1.3 2>> GC03-error.log 1>> GC03.log

quirks:
channels are sequentially numbered as such in conditions file:

	s7:
		channel-1 400nm
		channel-2 420nm
		channel-3 450nm
		channel-4 530nm
		channel-5 630nm
		channel-6 660nm
		channel-7 735nm
	s10:
		channel-1 370nm
		channel-2 400nm
		channel-3 420nm
		channel-4 450nm
		channel-5 530nm
		channel-6 620nm
		channel-7 660nm
		channel-8 735nm
		channel-9 850nm
		channel-10 6500k
	dyna:
		channel-1 380nm
		channel-2 400nm
		channel-3 420nm
		channel-4 450nm
		channel-5 530nm
		channel-6 620nm
		channel-7 660nm
		channel-8 735nm
		channel-9 5700K
`
	fmt.Printf(use, os.Args[0], os.Args[0], os.Args[0])
}

var wavelengthsS7 = []string{
	"400nm",
	"420nm",
	"450nm",
	"530nm",
	"630nm",
	"660nm",
	"735nm",
}

var wavelengthsS10 = []string{
	"370nm",
	"400nm",
	"420nm",
	"450nm",
	"530nm",
	"620nm",
	"660nm",
	"735nm",
	"850nm",
	"6500k",
}

var wavelengthsDyna = []string{
	"380nm",
	"400nm",
	"420nm",
	"450nm",
	"530nm",
	"620nm",
	"660nm",
	"735nm",
	"5700K",
}

func getLightStatusFromDevice() (*lightStatus, error) {
	v := newLightStatusPtr()

	netClient := &http.Client{
		Timeout: time.Second * 10,
	}
	resp, err := netClient.Get(statusURL.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = v.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return v, nil
}

func setMany(values []int) (err error) {
	delim := ":"
	intensityQueryStringValues := strings.Trim(strings.Replace(fmt.Sprint(values), " ", delim, -1), "[]")
	q := url.Values{}
	q.Set("int", intensityQueryStringValues)
	intensityURL.RawQuery = q.Encode()
	netClient := &http.Client{
		Timeout: time.Second * 10,
	}
	_, err = netClient.Get(intensityURL.String())
	return err
}

func discoDebug() {
	ticker := time.NewTicker(time.Millisecond * 50)
	go func() {
		for range ticker.C {
			status, err := getLightStatusFromDevice()
			if err != nil {
				// should retry in 5 seconds because we couldn't contact the light.
				errLog.Println(err)
				time.Sleep(time.Second * 5)
				continue
			}
			fmt.Println(ctools.DecodeStructToLineProtocol("heliospectra2-disco", tags, status))
			// randomise some intensities.
			//status.TargetIntensities = make([]int64, len(status.Intensities))
			//for i := range status.Intensities {
			//	status.TargetIntensities[i] = rand.Int63n(999)
			//}
			//err = setMany(status.TargetIntensities)
			//if err != nil {
			//	errLog.Println(err)
			//}
		}
	}()
	select {}
}

// runStuff, should send values and write metrics.
// returns true if program should continue, false if program should retry
func runStuff(point *ctools.TimePoint) bool {

	status, err := getLightStatusFromDevice()
	if err != nil {
		// should retry in 5 seconds because we couldn't contact the light.
		time.Sleep(time.Second * 5)
		return false
	}
	allWavelengths := []float64{
		point.Wavelength370nm,
		point.Wavelength380nm,
		point.Wavelength400nm,
		point.Wavelength420nm,
		point.Wavelength450nm,
		point.Wavelength530nm,
		point.Wavelength620nm,
		point.Wavelength630nm,
		point.Wavelength660nm,
		point.Wavelength735nm,
		point.Wavelength850nm,
		point.Wavelength5700k,
		point.Wavelength6500k,

	}
	var intensities []int
	for _,v := range allWavelengths {
		if v != ctools.NullTargetFloat64 {
			value := ctools.Clamp(int(v*multiplier), 0, 1000)
			intensities = append(intensities, value)
		}
	}


	if point.Wavelength400nm != ctools.NullTargetFloat64 && point.Wavelength400nm >= 0{
		status.Wavelength400nmTarget = (point.Wavelength400nm*multiplier)/10.0
	}
	if point.Wavelength420nm != ctools.NullTargetFloat64 && point.Wavelength420nm >= 0{
		status.Wavelength420nmTarget = (point.Wavelength420nm*multiplier)/10.0
	}
	if point.Wavelength450nm != ctools.NullTargetFloat64 && point.Wavelength450nm >= 0{
		status.Wavelength450nmTarget = (point.Wavelength450nm*multiplier)/10.0
	}
	if point.Wavelength530nm != ctools.NullTargetFloat64 && point.Wavelength530nm >= 0{
		status.Wavelength530nmTarget = (point.Wavelength530nm*multiplier)/10.0
	}
	if point.Wavelength630nm != ctools.NullTargetFloat64 && point.Wavelength630nm >= 0{
		status.Wavelength630nmTarget = (point.Wavelength630nm*multiplier)/10.0
	}
	if point.Wavelength660nm != ctools.NullTargetFloat64 && point.Wavelength660nm >= 0{
		status.Wavelength660nmTarget = (point.Wavelength660nm*multiplier)/10.0
	}
	if point.Wavelength735nm != ctools.NullTargetFloat64 && point.Wavelength735nm >= 0{
		status.Wavelength735nmTarget = (point.Wavelength735nm*multiplier)/10.0
	}

	if point.Wavelength370nm != ctools.NullTargetFloat64 && point.Wavelength370nm >= 0{
		status.Wavelength370nmTarget = (point.Wavelength370nm*multiplier)/10.0
	}
	if point.Wavelength620nm != ctools.NullTargetFloat64 && point.Wavelength620nm >= 0{
		status.Wavelength620nmTarget = (point.Wavelength620nm*multiplier)/10.0
	}
	if point.Wavelength850nm != ctools.NullTargetFloat64 && point.Wavelength850nm >= 0{
		status.Wavelength850nmTarget = (point.Wavelength850nm*multiplier)/10.0
	}
	if point.Wavelength6500k != ctools.NullTargetFloat64 && point.Wavelength6500k >= 0{
		status.Wavelength6500kTarget = (point.Wavelength6500k*multiplier)/10.0
	}

	if point.Wavelength380nm != ctools.NullTargetFloat64 && point.Wavelength380nm >= 0{
		status.Wavelength380nmTarget = (point.Wavelength380nm*multiplier)/10.0
	}
	if point.Wavelength5700k != ctools.NullTargetFloat64 && point.Wavelength5700k >= 0{
		status.Wavelength5700kTarget = (point.Wavelength5700k*multiplier)/10.0
	}

	err = setMany(intensities)
	if err != nil {
		errLog.Println(err)
		return false
	}

	status.ExecutedTimepoint = true
	errLog.Printf("ran %s %+v", point.Datetime.Format(time.RFC3339), intensities)

	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("heliospectra2", tags, status); err != nil {
			errLog.Println(err)
			errLog.Println(ctools.DecodeStructToLineProtocol("heliospectra2", tags, status))
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}
	return true
}

func init() {
	var err error
	hostname := os.Getenv("NAME")
	errLog = log.New(os.Stderr, "[heliospectra2] ", log.Ldate|log.Ltime|log.Lshortfile)
	// get the local zone and offset

	flag.Usage = usage
	flag.BoolVar(&noMetrics, "no-metrics", false, "dont collect metrics")
	if tempV := strings.ToLower(os.Getenv("NO_METRICS")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			noMetrics = true
		} else {
			noMetrics = false
		}
	}

	flag.BoolVar(&dummy, "dummy", false, "dont send conditions to light")
	if tempV := strings.ToLower(os.Getenv("DUMMY")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			dummy = true
		} else {
			dummy = false
		}
	}

	flag.BoolVar(&loopFirstDay, "loop", false, "loop over the first day")
	if tempV := strings.ToLower(os.Getenv("LOOP")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			loopFirstDay = true
		} else {
			loopFirstDay = false
		}
	}

	flag.BoolVar(&disco, "disco", false, "turn the music up")
	if tempV := strings.ToLower(os.Getenv("DISCO")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			disco = true
		} else {
			disco = false
		}
	}

	flag.StringVar(&tags.Host, "host-tag", hostname, "host tag to add to the measurements")
	if tempV := os.Getenv("HOST_TAG"); tempV != "" {
		tags.Host = tempV
	}

	flag.StringVar(&tags.Group, "group-tag", "nonspc", "host tag to add to the measurements")
	if tempV := os.Getenv("GROUP_TAG"); tempV != "" {
		tags.Group = tempV
	}

	flag.StringVar(&tags.DataID, "did-tag", "", "data id tag")
	if tempV := os.Getenv("DID_TAG"); tempV != "" {
		tags.DataID = tempV
	}

	flag.StringVar(&conditionsPath, "conditions", "", "conditions file to")
	if tempV := os.Getenv("CONDITIONS_FILE"); tempV != "" {
		conditionsPath = tempV
	}
	flag.DurationVar(&interval, "interval", time.Minute*10, "interval to record metrics at")
	if tempV := os.Getenv("INTERVAL"); tempV != "" {
		interval, err = time.ParseDuration(tempV)
		if err != nil {
			errLog.Println("Couldnt parse interval from environment")
			errLog.Println(err)
		}
	}
	flag.Float64Var(&multiplier, "multiplier", 10.0, "multiplier for the light")
	if tempV := os.Getenv("MULTIPLIER"); tempV != "" {
		multiplier, err = strconv.ParseFloat(tempV, 64)
		if err != nil {
			errLog.Println("Couldnt parse multiplier from environment")
			errLog.Println(err)
		}
	}
	flag.Parse()

	if noMetrics && dummy {
		errLog.Println("dummy and no-metrics specified, nothing to do.")
		os.Exit(1)
	}
	if conditionsPath != "" && !dummy {
		if tags.Group == "nonspc" {
			tags.Group = "spc"
		}
	}

	//check if address is provided by
	if address = os.Getenv("ADDRESS"); address == "" {
		address = flag.Arg(0)
	}

	ip := net.ParseIP(address)
	if ip != nil {
		address = fmt.Sprintf("http://%s/", ip)
	}
	u, err := url.Parse(address)
	if err != nil {
		panic(err)
	}
	u.Scheme = "http"
	u.Path = ""
	//u.RawQuery = ""
	fmt.Println(u.String())
	statusURL, err = url.Parse(u.String())
	if err != nil {
		panic(err)
	}
	statusURL.Path = "status.xml"
	intensityURL, err = url.Parse(u.String())
	if err != nil {
		panic(err)
	}
	intensityURL.Path = "intensity.cgi"
	intensityURL.RawQuery = ""

	errLog.Printf("tags: %+v", tags)
	errLog.Printf("address: \t%s\n", address)
	errLog.Printf("statusURL: \t%s\n", statusURL.String())
	errLog.Printf("file: \t%s\n", conditionsPath)
	errLog.Printf("interval: \t%s\n", interval)
	errLog.Printf("disco: \t%t\n", disco)
}

func main() {
	if interval == time.Second*0 {
		var err error
		status, err := getLightStatusFromDevice()
		if err != nil {
			errLog.Println(err)
			return
		}
		fmt.Println(ctools.DecodeStructToLineProtocol("heliospectra2", tags, status))
		os.Exit(0)
	}
	if !noMetrics && (conditionsPath == "" || dummy) && !disco {
		runMetrics := func() {
			status, err := getLightStatusFromDevice()
			if err != nil {
				errLog.Println(err)
				return
			}
			fmt.Println(ctools.DecodeStructToLineProtocol("heliospectra2", tags, status))
			for x := 0; x < 5; x++ {
				if err := ctools.WriteMetricUDP("heliospectra2", tags, status); err != nil {
					errLog.Println(err)
					time.Sleep(200 * time.Millisecond)
					continue
				}
				break
			}
		}

		runMetrics()

		ticker := time.NewTicker(interval)
		go func() {
			for range ticker.C {
				runMetrics()
			}
		}()
		select {}
	}
	if disco {
		discoDebug()
	}
	if conditionsPath != "" && !dummy {
		ctools.RunConditions(errLog, runStuff, conditionsPath, loopFirstDay)
	}
}
