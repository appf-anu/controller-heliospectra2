ARG GOLANG_VERSION=1.14

FROM golang:$GOLANG_VERSION AS builder

# Download and install the latest release of dep
RUN curl -s https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# Copy the code from the host and compile it
WORKDIR $GOPATH/src/gitlab.com/$CI_PROECT_PATH
COPY . ./

ARG DEP_ARGS='--vendor-only'
RUN dep ensure $DEP_ARGS

RUN CGO_ENABLED=0 GOOS=linux go build -v -o /main .

FROM alpine:3.12
RUN apk add --no-cache tzdata
COPY --from=builder /main ./
ENTRYPOINT ["./main"]